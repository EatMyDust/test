<?php
class M_Users
{	
	private static $instance;	
	private $msql;				
	private $sid;				
	private $uid;				
	private $onlineMap;			
	
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_Users();
			
		return self::$instance;
	}

	public function __construct()
	{
		$this->msql = M_MSQL::Instance();
		$this->sid = null;
		$this->uid = null;
		$this->onlineMap = null;
	}
	
	public function Get($id_user = null)
	{	
		//if dont have id_user get current
		if ($id_user == null)
			$id_user = $this->GetUid();
			
		if ($id_user == null)
			return null;
			
		//back user info by id
		$t = "SELECT * FROM users WHERE id_user = '%d'";
		$query = sprintf($t, $id_user);
		$result = $this->msql->Select($query);
		return $result[0];		
	}
	
	//clear existing sessions
	public function ClearSessions()
	{
		$min = date('Y-m-d H:i:s', time() - 60 * 20); 			
		$t = "time_last < '%s'";
		$where = sprintf($t, $min);
		$this->msql->Delete('sessions', $where);
	}

	//login method
	public function Login($login, $password, $remember = true)
	{
		$error = array();
		$result = array();
		
		$user = $this->GetByLogin($login);
		if(isset($user[0]))
		{
			$user = $user[0];
			if ($user['password'] != md5($password))
				$result['errors'] = "Не правильный логин или пароль";
		}else{
			$result['errors'] = "Не правильный логин или пароль";
		}
		
		if(count($result['errors'])<=0)
		{
			$id_user = $user['id_user'];
			
			if ($remember)
			{
				$expire = time() + 3600 * 24 * 100;
				setcookie('login', $login, $expire);
				setcookie('password', md5($password), $expire);
			}		
					
			$this->sid = $this->OpenSession($id_user);
		}
		
		return $result;
	}
	
	//logout method
	public function Logout()
	{
		setcookie('login', '', time() - 1);
		setcookie('password', '', time() - 1);
		unset($_COOKIE['login']);
		unset($_COOKIE['password']);
		unset($_SESSION['sid']);		
		$this->sid = null;
		$this->uid = null;
	}
						
	//get user by login
	public function GetByLogin($login)
	{	
		$result = array();
		$query = "SELECT * FROM users WHERE login = '%s'";
		$query = sprintf($query, $login);
		$result = $this->msql->Select($query);
		return $result;
	}
	
	//get current UID
	public function GetUid()
	{	
		if ($this->uid != null)
			return $this->uid;	

		$sid = $this->GetSid();
				
		if ($sid == null)
			return null;
			
		$query = "SELECT id_user FROM sessions WHERE sid = '%s'";
		$query = sprintf($query, mysql_real_escape_string($sid));
		$result = $this->msql->Select($query);
				
		if (count($result) == 0)
			return null;
			
		$this->uid = $result[0]['id_user'];
		return $this->uid;
	}

	
	public function isAdmin()
	{		
		$result = false;
		$id_user = $this->GetUid();
		    
		if ($id_user == null)
		    return $result;
		    
		$query = "SELECT `is_admin` FROM `users` 
			  WHERE id_user = '%d'";
	
		$query  = sprintf($query, $id_user);
		$arRes = $this->msql->Select($query);
		
		if($arRes[0]['is_admin'] == "1")
		{
			$result = true;
		}
		return $result;
	}
	
	
	//get current SID
	private function GetSid()
	{
		if ($this->sid != null)
			return $this->sid;
	
		$sid = null;
		
		// looking for sid in session
		if(isset($_SESSION['sid']))
			$sid = $_SESSION['sid'];
		
		//if colund find trying to find time_last in database
		if ($sid != null)
		{
			$session = array();
			$session['time_last'] = date('Y-m-d H:i:s'); 			
			$t = "sid = '%s'";
			$where = sprintf($t, mysql_real_escape_string($sid));
			$affected_rows = $this->msql->Update('sessions', $session, $where);

			if ($affected_rows == 0)
			{
				$t = "SELECT count(*) FROM sessions WHERE sid = '%s'";		
				$query = sprintf($t, mysql_real_escape_string($sid));
				$result = $this->msql->Select($query);
				
				if ($result[0]['count(*)'] == 0)
					$sid = null;			
			}			
		}		
		
		if ($sid == null && isset($_COOKIE['login']))
		{
			$user = $this->GetByLogin($_COOKIE['login']);
			
			if ($user != null && $user['password'] == $_COOKIE['password'])
				$sid = $this->OpenSession($user['id_user']);
		}
		
		if ($sid != null)
			$this->sid = $sid;
		
		return $sid;		
	}
	
	//open new session
	private function OpenSession($id_user)
	{
		//generate SID
		$sid = $this->GenerateStr(10);
				
		//put sid to database
		$now = date('Y-m-d H:i:s'); 
		$session = array();
		$session['id_user'] = $id_user;
		$session['sid'] = $sid;
		$session['time_start'] = $now;
		$session['time_last'] = $now;				
		$this->msql->Insert('sessions', $session); 
				
		//register sid in session
		$_SESSION['sid'] = $sid;				
				
		return $sid;	
	}

	//generate string function
	private function GenerateStr($length = 10) 
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
		$code = "";
		$clen = strlen($chars) - 1;  

		while (strlen($code) < $length) 
            $code .= $chars[mt_rand(0, $clen)];  

		return $code;
	}
}
