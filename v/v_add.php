<? if(!isset($arResult['success'])): ?>
	<? if(isset($arResult['errors']) && count($arResult['errors'])>0): ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="errors_block">
					<? foreach($arResult['errors'] as $error): ?>
						<p><?=$error;?></p>
					<? endforeach; ?>
				</div>
			</div>
		</div>
	<? endif;?>
	<div class="row">
		<div class="col-xs-12">
			<form method="POST" action="/123/index.php?q=tasks/add">
				<div class="form-group">
					<label for="name">Имя пользователя:</label>
					<input type="text" class="form-control" id="name" name="name" value="<?=isset($arResult['data']['name'])?$arResult['data']['name']:'';?>">
				</div>
				<div class="form-group">
					<label for="email">Email:</label>
					<input type="text" class="form-control" id="email" name="email" value="<?=isset($arResult['data']['email'])?$arResult['data']['email']:'';?>">
				</div>
				
				<div class="form-group">
					<label for="text">Текст задачи:</label>
					<textarea class="form-control" id="text" name="content"><?=isset($arResult['data']['content'])?$arResult['data']['content']:'';?></textarea>
				</div>
				
				<button type="submit" class="btn btn-primary">Добавить задачу</button>
			</form> 
		</div>
	</div>
<? else: ?>
	<div class="row">
		<div class="col-xs-12">
			<?=$arResult['success'];?>
		</div>
	</div>
<? endif; ?>
<div class="row">
	<div class="col-xs-12">
		<div class="home_button">
			<a href="<?=BASE_URL;?>">Вернуться на главную</a>
		</div>
	</div>
</div>