<? if(isset($arResult['errors']) && count($arResult['errors'])>0): ?>
	<div class="row">
		<div class="col-xs-12">
			<div class="errors_block">
				<? foreach($arResult['errors'] as $error): ?>
					<p><?=$error;?></p>
				<? endforeach; ?>
			</div>
		</div>
	</div>
<? endif;?>
<div class="row">
	<div class="col-xs-12">
		<form method="POST">
			<div class="form-group">
				<label for="login">Логин:</label>
				<input type="login" class="form-control" id="login" name="login">
			</div>
			<div class="form-group">
				<label for="pwd">Пароль:</label>
				<input type="password" class="form-control" id="pwd" name="password">
			</div>
			<div class="form-group form-check">
				<label class="form-check-label">
					<input class="form-check-input" type="checkbox" name="remember"> Запомнить меня
				</label>
			</div>
			<button type="submit" class="btn btn-primary">Войти</button>
		</form> 
	</div>
</div>